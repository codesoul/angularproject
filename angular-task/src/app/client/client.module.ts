import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ClientBillingComponent } from './client-billing/client-billing.component';
import { RouterModule, Routes } from '@angular/router';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';

const routes: Routes = [
{
  path: '',
  component: ClientBillingComponent
}
]


@NgModule({
  declarations: [
    ClientBillingComponent,
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    MatSlideToggleModule
  ]
})
export class ClientModule { }
